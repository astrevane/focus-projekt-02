import { Component, OnInit } from '@angular/core';
import { CallService } from '../call.service'

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.css']
})
export class WidgetComponent implements OnInit {
  number: string
  number2: string
  validator = /(^[0-9]{9}$)/
  state: string = "waiting"
  interval: number
  touched: boolean = false
  touched2: boolean = false

  constructor(private callService: CallService) { }

  call() {
    if (this.isValidNumber(true) && this.isValidNumber2(true)) {
      this.callService.placeCall(this.number, this.number2)
      this.state = "ringing"
      this.callService.getCallId().subscribe(callId => {
        this.checkStatus()
      })
    }
  }


  isValidNumber(change: boolean): Boolean {
    if (change) {
      this.touched = true
    }
    if (!this.touched) {
      return true
    }
    return this.validator.test(this.number)
  }

  isValidNumber2(change: boolean): Boolean {
    if (change) {
      this.touched2 = true
    }
    if (!this.touched2) {
      return true
    }
    return this.validator.test(this.number2)
  }

  checkStatus() {
    this.callService.getCallStatus().subscribe(state => {
      this.state = state
    })
  }




  ngOnInit() {
  }
}
