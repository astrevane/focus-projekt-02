# Instrukcja

Aby aplikacja funkcjonowała poprawnie należy uruchomić backend tj. aplikację oddaną jako projekt 1.

## Pobranie z repozytorium

Aby pobrać aplikację z repozytorium otwieramy terminal i używamy polecenia `git clone https://astrevane@bitbucket.org/astrevane/focus-projekt-02.git`. Polecenie automatycznie utworzy folder o nazwie `focus-projekt-02` i umieści w nim pliki projektu, przechodzimy do tego folderu poleceniem `cd focus-projekt-02`.

## Pobranie niezbędnych modułów

Pobieramy wymagane moduły poleceniem `npm i`. 

## Uruchomienie aplikacji

Uruchamiamy aplikację poleceniem `ng serve`.

## Użytkowanie aplikacji

Aby skorzystać z aplikacji otwieramy przeglądarkę i przechodzimy na adres `http://localhost:4200/`. Następnie w polach znajdujących się na lewo od przycisku `Zadzwoń`, wpisujemy numery telefonów pomiędzy którymi ma zostać nawiązane połączenie i jeśli pola z numerami nie świecą się na czerwono, klikamy na przycisk `Zadzwoń` (w innym wypadku numery nie przechodzą walidacji). Poniżej pól z numerami wyświetlany jest aktualny status połączenia.
